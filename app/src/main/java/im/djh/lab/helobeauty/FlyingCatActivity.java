package im.djh.lab.helobeauty;

import java.lang.ref.WeakReference;
import java.util.Random;

import android.animation.TimeAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.util.Log;

/**
 * 彩蛋（根据android源码修改）
 *
 * @author zony
 * @time 11 Dec 2014 12:50:01
 * @improve djh
 * @improveTime 015 Apr. 2016 10:40
 */
@SuppressLint("NewApi")
public class FlyingCatActivity extends Activity {

    public static class Board extends FrameLayout {
        // 控制数量
        private static final int NUM_CATS = 12;
        private static Random sRNG = new Random();

        private static float lerp(float a, float b, float f) {
            return (b - a) * f + a;
	}

        private static float randfrange(float a, float b) {
            return lerp(a, b, sRNG.nextFloat());
        }

	// private static int randsign() {
        //     return sRNG.nextBoolean() ? 1 : -1;
        // }
        // private static <E> E pick(E[] array) {
        //     if (array.length == 0)
        //         return null;
        //     return array[sRNG.nextInt(array.length)];
        // }
	
        public class FlyingCat extends ImageView {
            private static final float VMAX = 300.0f;
            private static final float VMIN = 50.0f;

            private float v;
            private float z;
            // public ComponentName component;

            public FlyingCat(Context context, AttributeSet as) {
                super(context, as);
                setImageResource(R.drawable.star_anim);
            }

            public void reset() {
                final float scale = lerp(0.8f, 1.2f, z);
                setScaleX(scale);
                setScaleY(scale);
                // setX(randfrange(0, Board.this.getHeight() - scale * getHeight()));
                // setY(randfrange(0, Board.this.getHeight() - scale * getHeight()));
                setY(0f); // Start from status bar
                v = lerp(VMIN, VMAX, z); // NOTE: v is speed, z is current index num during item generate
            }

            public void update(float dt) {
                // v = 50 * dt * dt; // v = a * t, physics stuff
                // 根据Y轴漂移
                setY(getY() + v * dt * 3); // 3 is MAGIC
                // Log.v("dt", String.valueOf(dt));
                // Log.v("v", String.valueOf(v));
            }
        }

        TimeAnimator mAnim;
        Context mContext;
        private static final float BOARDPADDING = 100f;

        public Board(Context context, AttributeSet as) {
            super(context, as);
            this.mContext = context;
        }

        private void reset() {
            removeAllViews();
            final ViewGroup.LayoutParams wrap = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            for (int i = 0; i < NUM_CATS; i++) {  // Init all items to anime
                FlyingCat nv = new FlyingCat(getContext(), null);
                addView(nv, wrap);
                nv.z = ((float) i / NUM_CATS);
                nv.z *= nv.z;
                nv.reset();
                nv.setX(randfrange(BOARDPADDING, Board.this.getWidth() - BOARDPADDING));  // Random at X offset
                final AnimationDrawable anim = (AnimationDrawable) nv.getDrawable();
                postDelayed(new Runnable() {
                    public void run() {
                        anim.start();
                    }
		    }, (int) randfrange(0, 1000));  // Start animation at different time
            }
            if (mAnim != null) {
                mAnim.cancel();
            }
            mAnim = new TimeAnimator();
            mAnim.setTimeListener(new TimeAnimator.TimeListener() {
                public void onTimeUpdate(TimeAnimator animation, long totalTime, long deltaTime) {
                    for (int i = 0; i < getChildCount(); i++) {
                        View v = getChildAt(i);
                        if (!(v instanceof FlyingCat))
                            continue;
                        FlyingCat nv = (FlyingCat) v;
                        nv.update(deltaTime / DROPFUSE);  // Redraw on time pass
                    }
                }
            });
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            post(new Runnable() {
                public void run() {
                    reset();
                    mAnim.start();
                }
            });
        }

        @Override
        protected void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            mAnim.cancel();
        }

        @Override
        public boolean isOpaque() {
            return true;
        }
    }

    private Board mBoard;
    private MyHandler mHandler;
    private static final int ANIM_DURATION = 2500;
    private static final float DROPFUSE = 200f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBoard = new Board(this, null);
        setContentView(mBoard);
        mHandler = new MyHandler(this);
        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                finish();
                mHandler.releaseHandler();
            }
	    }, ANIM_DURATION);  // Anime stop when hits ANIM_DURATION
    }

    protected static class MyHandler extends Handler {
        WeakReference<FlyingCatActivity> reference;

        MyHandler(FlyingCatActivity layout) {
            reference = new WeakReference<FlyingCatActivity>(layout);
        }

        /**
         * 退出时手动释放
         *
         * @author zony
         * @time 11 Dec 2014 12:41:39
         */
        public void releaseHandler() {
            if (reference != null) {
                reference.clear();
                reference = null;
            }
        }

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (reference == null) {
                return;
            }
            FlyingCatActivity theLayout = reference.get();
            if (theLayout == null) {
                return;
            }
        }
    };

    // @Override
    // public void onUserInteraction() {
    // finish();
    // }
}
